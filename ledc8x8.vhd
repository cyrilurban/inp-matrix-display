-- file:	ledc8x8.vhd
-- author:	CYRIL URBAN
-- date:	24.10.2016
-- brief:	rizeni maticoveho displeji

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity ledc8x8 is
port ( 

		ROW: out std_logic_vector(7 downto 0);
		LED: out std_logic_vector(7 downto 0);
		RESET: in std_logic;
		SMCLK: in std_logic

);
end ledc8x8;

architecture main of ledc8x8 is
	
	signal clock_enable: std_logic := '0';
	signal switch_first_last_name: std_logic := '0';
	signal counter: std_logic_vector(23 downto 0);
	signal first_name: std_logic_vector(7 downto 0) := "00000000";
	signal last_name: std_logic_vector(7 downto 0) := "00000000";
	signal row_active: std_logic_vector(7 downto 0) := "10000000";
	
begin


	-- citac pro zmenu aktivace radku a pro prepinani mezi pismeny

	process (SMCLK, RESET)

	begin
		if RESET = '1'
		then
			counter <= "000000000000000000000000";
		elsif rising_edge(SMCLK)
		then
			if counter(7 downto 0) = "11111111" -- SMCLK/256 -> jednou za 256 taktu se clock_enable nastavi do jednicky (prepne se aktivita radku)
			then
				clock_enable <= '1';
			else
				clock_enable <= '0';
			end if;

			switch_first_last_name <= counter(23); -- ( (1/7 372 800) * 2^23 ) = 1.138 Hz -> prepinani pismen cca kazdou sekundu
			counter <= counter + 1;
		end if;
	end process;


 	-- rotační registr -> kdyz bude clock_enable = 1 (jednou za 256 taktu), prepne se aktivita radku na dalsi
	
	process (SMCLK, RESET, row_active)
	begin

		if RESET = '1'
		then

			ROW <= "10000000";
			row_active <= "10000000";

		elsif rising_edge(SMCLK) AND clock_enable = '1'
		then
			case row_active is
				
				when "00000001" => row_active <= "10000000";
				when "00000010" => row_active <= "00000001";
				when "00000100" => row_active <= "00000010";
				when "00001000" => row_active <= "00000100";
				when "00010000" => row_active <= "00001000";
				when "00100000" => row_active <= "00010000";
				when "01000000" => row_active <= "00100000";
				when "10000000" => row_active <= "01000000";
				when others => null;

			end case;
		end if;

		ROW <= row_active;
	end process;


	-- 1. dekodér -> urci jake LED rosvitit pro jednotlive radky (1. pismeno z krestniho jmena)

	process (SMCLK, first_name)
	begin
		if rising_edge(SMCLK)
		then
			case row_active is
				when "00000001" => first_name <= "11000011";
				when "00000010" => first_name <= "10000001";
				when "00000100" => first_name <= "10011001";
				when "00001000" => first_name <= "11111001";
				when "00010000" => first_name <= "11111001";
				when "00100000" => first_name <= "10011001";
				when "01000000" => first_name <= "10000001";
				when "10000000" => first_name <= "11000011";
				when others => null;
			end case;
		end if;
	end process;


	-- 2. dekodér -> urci jake LED rosvitit pro jednotlive radky (1. pismeno z prijmeni)

	process (SMCLK, last_name)
	begin
		if rising_edge(SMCLK)
		then
			case row_active is
				when "00000001" => last_name <= "10011001";
				when "00000010" => last_name <= "10011001";
				when "00000100" => last_name <= "10011001";
				when "00001000" => last_name <= "10011001";
				when "00010000" => last_name <= "10011001";
				when "00100000" => last_name <= "10011001";
				when "01000000" => last_name <= "10000001";
				when "10000000" => last_name <= "11000011";
				when others => null;
			end case;
		end if;
	end process;


	-- prepinani mezi prvnim a druhym pismenem cca kazdou sukundu (1Hz)

	process (SMCLK, RESET, first_name, last_name, switch_first_last_name)
	begin
		if RESET = '1' then
			LED <= first_name;

		elsif rising_edge(SMCLK)
		then
			if switch_first_last_name = '0'
			then
				LED <= first_name;
			else
				LED <= last_name;
			end if;
		end if;
	end process;

end main;
